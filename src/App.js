import avatar from './assets/images/48.jpg'
import './App.css'
import 'bootstrap/dist/css/bootstrap.min.css'
function App() {
  return (
    <div className='container dc-container'>
      <div>
        <img src={avatar} alt="img-avatar" className='dc-avatar'/>
      </div>
      <div className='dc-quote'>
        This is one of the best developer blogs on the planet! I read it,daily to improve my skills.
      </div>
      <div>
        <span className='dc-name'>
          Tammy stevents
        </span>
        <span>
        &nbsp; * &nbsp;Front End developer
        </span>
      </div>
    </div>
  );
}

export default App;
